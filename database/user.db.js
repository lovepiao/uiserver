var config = require("./config.db");
var ret = require("../routes/result");

class User {
  constructor() {
    this.db = new config({
      tableName: "user"
    });

    this.db.connectDataBase();
  }

  login(username, password, callback) {
    let _self = this;
    if (!username || !password) {
      return callback(ret(1104));
    }
    _self.db
      .sql(
        `select * from ${_self.db.tableName} where name = ?`,
        username,
        "get"
      )
      .then(res => {
        if (res && res.password == password) {
          callback(ret(100));
        } else {
          callback(ret(res ? 1106 : 1105));
        }
      })
      .catch(err => {
        console.log(err);
        callback(ret(1107));
      });
  }

  userlist(callback) {
    let _self = this;
    _self.db
      .sql(`select * from ${_self.db.tableName}`, [], "all")
      .then(res => {
        let ulist = [];
        res.forEach(ele => {
          ulist.push(ele.name);
        });
        callback(ulist);
      })
      .catch(err => {
        console.log(err);
        callback(ret(1107));
      });
  }

  regist(username, password, ip, callback) {
    let _self = this;
    if (!username || !password) {
      return callback(ret(1104));
    }
    _self.db
      .sql(
        `select * from ${_self.db.tableName} where name = ?`,
        username,
        "get"
      )
      .then(res => {
        if (res) {
          callback(ret(1111));
        } else {
          _self.db
            .sql(
              `insert into ${
                _self.db.tableName
              } (name, password, regip, iplock) values(?,?,?,?)`,
              [username, password, ip, false]
            )
            .then(res => {
              if (res) {
                _self.db.sql(`insert into auth (user, dir) values(?,?)`, [
                  username,
                  ""
                ]);
                callback(ret(102));
              } else {
                callback(ret(1110));
              }
            })
            .catch(err => {
              console.log(err);
              callback(ret(1107));
            });
        }
      })
      .catch(err => {
        console.log(err);
        callback(ret(1110));
      });
  }

  chgpwd(username, opwd, npwd, ip, callback) {
    let _self = this;
    if (!username || !opwd || !npwd) {
      return callback(ret(1104));
    }

    _self.db
      .sql(
        `select * from ${_self.db.tableName} where name = ?`,
        username,
        "get"
      )
      .then(res => {
        if (res && res.password == opwd) {
          if (res.regip == ip) {
            _self.db
              .sql(
                `update ${_self.db.tableName} set password = ? where name = ?`,
                [npwd, username]
              )
              .then(res => {
                callback(ret(103));
              })
              .catch(err => {
                console.log(err);
                callback(ret(1107));
              });
          } else {
            callback(ret(1113));
          }
        } else {
          callback(ret(1106));
        }
      })
      .catch(err => {
        console.log(err);
        callback(ret(1107));
      });
  }

  log(username, act, info) {
    let _self = this;
    _self.db.sql(`insert into log (id, active, info, time) values(?,?,?,?)`, [
      username,
      act,
      info,
      new Date().toLocaleString()
    ]);
  }
}
let user = new User();
module.exports = user;
