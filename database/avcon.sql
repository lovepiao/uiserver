--
-- 由SQLiteStudio v3.1.1 产生的文件 周三 7月 18 14:23:34 2018
--
-- 文本编码：UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- 表：auth
DROP TABLE IF EXISTS auth;

CREATE TABLE auth (
    user STRING (3, 18) NOT NULL,
    dir  STRING
);

INSERT INTO auth (user, dir) VALUES ('ncavcon', '*');
INSERT INTO auth (user, dir) VALUES ('web', '*');
INSERT INTO auth (user, dir) VALUES ('phone', '*');

-- 表：log
DROP TABLE IF EXISTS log;

CREATE TABLE log (
    id     STRING (3, 18) NOT NULL,
    active STRING,
    info   STRING,
    time   STRING
);

INSERT INTO log (id, active, info, time) VALUES ('ncavcon', 'create', '创建表格', '0000-00-00 00:00:00');

-- 表：user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    name     STRING (3, 18) NOT NULL,
    password STRING (3, 18) NOT NULL,
    regip    STRING,
    iplock   BOOLEAN
);

INSERT INTO user (name, password, regip, iplock) VALUES ('phone', 'phone', NULL, NULL);
INSERT INTO user (name, password, regip, iplock) VALUES ('web', 'web', NULL, NULL);
INSERT INTO user (name, password, regip, iplock) VALUES ('ncavcon', 'ncavcon', NULL, NULL);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
