var config = require("./config.db");
var fs = require("fs");
var path = require("path");

/**
 * 静态页面根目录
 */
let webdir = "D:/UI";//"/Volumes/UI"

class Auth {
  constructor() {
    this.db = new config({
      tableName: "auth"
    });

    this.dirs = [];
    this.db.connectDataBase();
  }

  getAuth(username, callback) {
    let _self = this;
    if (username) {
      _self.db
        .sql(
          `select * from ${_self.db.tableName} where user = ?`,
          username,
          "get"
        )
        .then(res => {
          callback(res ? res.dir : "");
        })
        .catch(err => {
          console.log(err);
          callback("");
        });
    } else {
      callback("");
    }
  }

  setDir(username, dirs, callback) {
    let _self = this;
    if (username) {
      _self.db
        .sql(`update ${_self.db.tableName} set dir = ? where user = ?`, [
          dirs,
          username
        ])
        .then(res => {
          callback(res);
        })
        .catch(err => {
          console.log(err);
          callback(false);
        });
    } else {
      callback(false);
    }
  }

  getDir(username, callback) {
    let _self = this;
    if (_self.dirs.length == 0) {
      let pa = fs.readdirSync(webdir);
      pa.forEach(ele => {
        let info = fs.statSync(webdir + "/" + ele);
        if (info.isDirectory()) {
          _self.dirs.push(ele);
        }
      });
    }

    _self.getAuth(username, auth => {
      if (auth == "*") {
        callback(_self.dirs);
      } else {
        auth = auth ? JSON.parse(auth) : [];
        callback(auth);
      }
    });
  }

  toIndexFile(dir){
    return webdir+'/'+dir;
  }

  log(username, act, info) {
    let _self = this;
    _self.db.sql(
      `insert into log (id, active, info, time) values(?,?,?,?)`,
      [username, act, info ,new Date().toLocaleString()]
    );
  }
}
let auth = new Auth();
module.exports = auth;
