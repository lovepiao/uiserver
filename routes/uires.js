var express = require("express");
var router = express.Router();
var dir = require("../database/dir.db");
var ret = require("./result");
let isResAuth = ret.isResAuth;
/**
 * 获取登录用户资源目录
 */
router.post("/", function(req, res, next) {
  if (!req.session.loginuser) {
    return res.redirect("/");
  }
  dir.getDir(req.session.loginuser, auth => {
    res.json(auth);
  });
});

/**
 * 获取其它用户资源目录
 */
router.post("/get", (req, res, next) => {
  if (!req.session.loginuser) {
    return res.redirect("/");
  }

  isResAuth(req, auth => {
    if (auth) {
      dir.getDir(req.body.username, auth => {
        res.json(auth);
      });
    } else {
      res.json(ret(1200));
    }
  });
});

/**
 * 设置用户资源目录
 */
router.post("/set", (req, res, next) => {
  if (!req.session.loginuser) {
    return res.redirect("/");
  }

  isResAuth(req, auth => {
    if (auth) {
      dir.setDir(req.body.username, req.body.dirs, result => {
        res.json(ret(result ? 200 : 1201));
      });
    } else {
      res.json(ret(1200));
    }
  });
});

module.exports = router;
