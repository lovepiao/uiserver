var express = require("express");
var router = express.Router();
var user = require("../database/user.db");
var ret = require("./result");
let getClientIp = ret.getClientIp;
let invitecode = [];
/**
 * 登录
 */
router.post("/login", function(req, res, next) {
  if (req.session.loginuser) {
    return res.json(ret(1100));
  }

  user.login(req.body.username, req.body.password, function(result) {
    if (result.ret_code < ret.failedcode) {
      req.session.regenerate(function(err) {
        if (err) {
          return res.json(ret(1101));
        }
        req.session.loginuser = req.body.username;
        user.log(req.session.loginuser, "login", getClientIp(req));
        res.json(result);
      });
    } else {
      res.json(result);
    }
  });
});

/**
 * 注销
 */
router.post("/logout", function(req, res, next) {
  // 备注：这里用的 session-file-store 在destroy 方法里，并没有销毁cookie
  // 所以客户端的 cookie 还是存在，导致的问题 --> 退出登陆后，服务端检测到cookie
  // 然后去查找对应的 session 文件，报错
  // session-file-store 本身的bug

  if (!req.session.loginuser) {
    return res.json(ret(1102));
  }

  user.log(req.session.loginuser, "logout", getClientIp(req));
  req.session.destroy(function(err) {
    if (err) {
      res.json(ret(1101));
      return;
    }
    //req.session.loginuser = null;//此时session对象已销毁
    res.clearCookie("web.html.ui.server");
    res.redirect("/");
  });
});

router.post("/userlist", function(req, res, next) {
  if (req.session.loginuser) {
    ret.isResAuth(req, auth => {
      if (auth) {
        user.userlist(result => {
          res.json(result);
        });
      } else {
        res.json(ret(1200));
      }
    });
  } else {
    res.json(ret(1102));
  }
});
/**
 * changepassword 修改密码
 */
router.post("/chgpwd", function(req, res, next) {
  if (req.session.loginuser) {
    user.log(req.session.loginuser, "chgpwd", getClientIp(req));
    user.chgpwd(
      req.session.loginuser,
      req.body.oldpassword,
      req.body.newpassword,
      getClientIp(req),
      result => {
        res.json(result);
      }
    );
  } else {
    res.json(ret(1102));
  }
});

router.post("/reg", function(req, res, next) {
  if (!req.session.loginuser) {
    if (invitecode.indexOf(req.body.invitecode) < 0) {
      res.json(ret(1109));
    } else {
      user.regist(
        req.body.username,
        req.body.password,
        getClientIp(req),
        function(result) {
          if (result.ret_code < ret.failedcode) {
            invitecode.splice(invitecode.indexOf(req.body.invitecode), 1);
          }
          user.log(req.session.loginuser, "regist", getClientIp(req));
          res.json(result);
        }
      );
    }
  } else {
    res.json(ret(1103));
  }
});

router.post("/clearcode", function(req, res, next) {
  if (req.session.loginuser) {
    ret.isResAuth(req, auth => {
      if (auth) {
        invitecode = [];
        res.json(ret(101));
      } else {
        res.json(ret(1200));
      }
    });
  } else {
    res.json(ret(1102));
  }
});

router.post("/invitecode", function(req, res, next) {
  if (req.session.loginuser) {
    ret.isResAuth(req, auth => {
      if (auth) {
        let ic = Math.floor(Math.random() * 9000 + 1000);
        if (invitecode.indexOf(ic) < 0) {
          invitecode.push(ic);
        }
        res.json({ code: ic, list: invitecode });
      } else {
        res.json(ret(1200));
      }
    });
  } else {
    res.json(ret(1102));
  }
});

router.post("/icodelist", function(req, res, next) {
  if (req.session.loginuser) {
    ret.isResAuth(req, auth => {
      if (auth) {
        res.json(invitecode);
      } else {
        res.json(ret(1200));
      }
    });
  } else {
    res.json(ret(1102));
  }
});

router.post("/info", function(req, res, next) {
  if (req.session.loginuser) {
    ret.isResAuth(req, auth => {
      res.json({ id: req.session.loginuser, auth: auth ? 1 : 0 });
    });
  } else {
    res.json({ id: "", auth: 0 });
  }
});

module.exports = router;
