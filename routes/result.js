let dir = require("../database/dir.db");

let ret = {
  "100": "登录成功",
  "101": "已清空邀请码",
  "102": "注册成功",
  "103": "密码修改成功",

  "200": "权限修改成功",

  "1100": "您已登录，请注销后重试",
  "1101": "服务器异常(101)，请稍后重试",
  "1102": "您当前未登录",
  "1103": "您已登录，请注销后重试",
  "1104": "账号或密码不能为空",
  "1105": "账号不存在",
  "1106": "密码错误",
  "1107": "服务器异常(107)，请稍后重试",
  "1108": "暂不支持注册",
  "1109": "请使用有效的邀请码注册",
  "1110": "账号注册失败，请稍后重试",
  "1111": "该账号已存在",
  "1112": "登录超时，请稍后重试",
  "1113": "当前登录密码与注册密码不同",

  "1200": "权限不足",
  "1201": "服务器异常(201)，请稍后重试",

  end: "end"
};

function result(ret_code) {
  return { ret_code, ret_err: ret[String(ret_code)] };
}

/**
 * 获取请求的客户端ip地址
 * @param {请求} req
 */
result.getClientIp = function(req) {
  if (req) {
    if (req.headers && req.headers["x-forwarded-for"]) {
      return req.headers["x-forwarded-for"];
    } else if (req.socket && req.socket.remoteAddress) {
      return req.socket.remoteAddress;
    } else if (req.connection) {
      if (req.connection.remoteAddress) {
        return req.connection.remoteAddress;
      } else if (req.connection.socket && req.connection.socket.remoteAddress) {
        return req.connection.socket.remoteAddress;
      }
    }
  }
  return "";
};

let ownip = ["192.168.0.156", "192.168.0.152"];
result.isResAuth = (req, callback) => {
  let clientIp = String(result.getClientIp(req));
  for (let i = 0; i < ownip.length; ++i) {
    if (clientIp.indexOf(ownip[i]) >= 0) {
      dir.getAuth(req.session.loginuser, auth => {
        callback(auth == "*");
      });
      return;
    }
  }
  return callback(false);
};

result.failedcode = 1000;
module.exports = result;
