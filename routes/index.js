var express = require("express");
var router = express.Router();
var dres = require("../database/dir.db");

/* GET home page. */
router.post("/main", (req, res) => {
  dres.getDir("", () => {});
  res.json({ dir: dres.dirs });
});

router.get("/*", function(req, res, next) {
  var param = req.params[0];
  if (!param) {
    res.sendFile(dres.toIndexFile(""));
  } else {
    if (!req.session.loginuser) {
      return res.redirect("/");//如果示例页面不需要权限判断可注释这句
    }
    res.sendFile(dres.toIndexFile(param));
  }
});

module.exports = router;
