var createError = require("http-errors");
var express = require("express");
var path = require("path");
var session = require("express-session");
var FileStore = require("session-file-store")(session);
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var uiRouter = require("./routes/uires");

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser("web.html.ui"));
app.use(express.static(path.join(__dirname, "public")));

app.use(
  session({
    name: "web.html.ui.server",
    secret: "web.html.ui", // 用来对session id相关的cookie进行签名
    store: new FileStore(), // 本地存储session（文本文件，也可以选择其他store，比如redis的）
    saveUninitialized: false, // 是否自动保存未初始化的会话，建议false
    resave: false, // 是否每次都重新保存会话，建议false
    rolling: true
    // cookie: {
    //     maxAge: 10 * 1000  // 有效期，单位是毫秒
    // }
  })
);

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/uires", uiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.redirect("/");
});

module.exports = app;
