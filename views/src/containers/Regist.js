import Regist from "../components/Regist";
import { connect } from "react-redux";
import { USER_REGIST, saga } from "../redux/actions";

const mapStateToProps = state => ({
  err: state.user.regerr
});

const mapDispatchToProps = dispatch => ({
  onRegist: (user, password, agepassword, vercode) => {
    dispatch(saga(USER_REGIST, { user, password, agepassword, vercode }));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Regist);
