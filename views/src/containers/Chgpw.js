import Chgpw from "../components/ChangePassWord";
import { connect } from "react-redux";
import { USER_CHANGEPW, saga } from "../redux/actions";

const mapStateToProps = state => ({
  err: state.user.chgpwerr,
  blogin: state.user.user
});

const mapDispatchToProps = dispatch => ({
  onChgPw: (oldpassword, newpassword, agepassword) => {
    dispatch(saga(USER_CHANGEPW, { oldpassword, newpassword, agepassword }));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chgpw);
