import ChgAuth from "../components/ChangeAuth";
import { connect } from "react-redux";
import {
  USER_GETINVITE,
  USER_CLEARINVITE,
  USER_UPDATEINVITE,
  saga,
  USER_UPDATELIST,
  DIRS_LOOK,
  DIRS_CHANGE
} from "../redux/actions";

const mapStateToProps = state => ({
  cinvitecode: state.user.invitecode,
  invitecodelist: state.user.invitecodelist,
  userlist: state.user.userlist,
  dirs: state.user.dirs,
  user: state.dirs.cuser,
  uauth: state.dirs.dirs,
  bauth: state.user.auth
});

const mapDispatchToProps = dispatch => ({
  onUpdateUserList: () => {
    dispatch(saga(USER_UPDATELIST));
  },
  onGetInviteCode: () => {
    dispatch(saga(USER_GETINVITE));
  },
  onUpdateInviteCode: () => {
    dispatch(saga(USER_UPDATEINVITE));
  },
  onClearInviteCode: () => {
    dispatch(saga(USER_CLEARINVITE));
  },
  onUpdateUserAuth: user => {
    dispatch(saga(DIRS_LOOK, { user }));
  },
  onChangeUserAuth: (user, auth) => {
    dispatch(saga(DIRS_CHANGE, { user, auth }));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChgAuth);
