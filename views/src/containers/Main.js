import React from "react";
import Login from "../components/Login";
import UIList from "../components/UIList";
import { connect } from "react-redux";
import { USER_LOGIN, USER_LOGOUT, saga } from "../redux/actions";

let Main = ({ user, auth, dirs, err, onLogin, onLogout }) => (
  <div>
    {user ? (
      <UIList user={user} auth={auth} dirs={dirs} onLogout={onLogout} />
    ) : (
      <Login err={err} onLogin={onLogin} />
    )}
  </div>
);

const mapStateToProps = state => ({
  user: state.user.user,
  auth: state.user.auth,
  dirs: state.user.dirs,
  err: state.user.err
});

const mapDispatchToProps = dispatch => ({
  onLogin: (user, password) => {
    dispatch(saga(USER_LOGIN, { user, password }));
  },
  onLogout: () => {
    dispatch(saga(USER_LOGOUT));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
