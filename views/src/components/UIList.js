/**
 * 资源列表
 */

import React from "react";
import { Link } from "react-router-dom";
import { Button, List } from "antd";

export default ({ user, auth, dirs, onLogout }) => {
  return (
    <div>
      <List
        header={
          <div style={{ textAlign: "center", margin: "0 10px" }}>
            <Button
              type="primary"
              onClick={e => {
                e.preventDefault();
              }}
              style={{
                width: "100px",
                display: auth ? "inline" : "none",
                margin: "0 10px"
              }}
            >
              <Link to="/chga">账号管理</Link>
            </Button>
            <Button
              type="primary"
              onClick={e => {
                e.preventDefault();
              }}
              style={{ width: "100px", margin: "0 10px" }}
            >
              <Link to="/chgpw">修改密码</Link>
            </Button>

            <Button
              type="primary"
              onClick={e => {
                e.preventDefault();
                onLogout();
              }}
              style={{ margin: "0 10px" }}
            >
              {"(" + user + ")"}退出
            </Button>
          </div>
        }
        bordered
        dataSource={dirs}
        renderItem={item => (
          <List.Item>
            <a
              href={"/" + item + "/index.html"}
              style={{ textAlign: "center", width: "100%" }}
            >
              {item}
            </a>
          </List.Item>
        )}
      />
    </div>
  );
};
