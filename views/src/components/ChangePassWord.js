/**
 * 修改密码
 */

import React from "react";
import { Form, Input, Icon, Button, Alert, Row } from "antd";
import { Redirect } from "react-router-dom";
import "./Login.less";

const FormItem = Form.Item;

const UserForm = ({ onChgPw, err, form }) => (
  <Form
    onSubmit={e => {
      e.preventDefault();
      form.validateFields((err, values) => {
        if (!err) {
          onChgPw(values.oldpassword, values.newpassword, values.agepassword);
        }
      });
    }}
    className="login-form"
  >
    {(() => {
      let items = [];
      const fields = ["oldpassword", "newpassword", "agepassword"];
      const msg = ["请输入旧密码!", "请输入新密码！", "请确认新密码！"];
      const places = ["旧密码", "新密码", "确认新密码"];
      for (let i = 0; i < 3; ++i) {
        items.push(
          <FormItem key={i}>
            {form.getFieldDecorator(fields[i], {
              rules: [{ required: true, message: msg[i] }]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25" }} />
                }
                type="password"
                placeholder={places[i]}
              />
            )}
          </FormItem>
        );
      }
      return items;
    })()}

    <FormItem>
      <Button type="primary" htmlType="submit" className="login-form-button">
        修改密码
      </Button>
    </FormItem>
    <Alert
      type="error"
      message={err ? err : "空内容占高"}
      style={{ opacity: err ? 1.0 : 0 }}
      banner
    />
  </Form>
);

const ChangePassWord = Form.create()(UserForm);

export default ({ err, blogin, onChgPw }) =>
  blogin ? (
    <Row
      type="flex"
      justify="space-around"
      align="middle"
      className="view-height"
    >
      <ChangePassWord err={err} onChgPw={onChgPw} />
    </Row>
  ) : (
    <Redirect to="/" />
  );
