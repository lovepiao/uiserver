/**
 * 修改权限
 */

import React from "react";
import { Button, List, Checkbox, Switch, Icon } from "antd";
import { Redirect } from "react-router-dom";

const CheckboxGroup = Checkbox.Group;

class ChangeAuth extends React.Component {
  constructor(props) {
    console.log(props);
    super(props);
    this.state = {
      userAuth: props.uauth,
      admin: false
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ userAuth: nextProps.uauth, admin: false });
  }

  render() {
    return this.props.bauth ? (
      <div>
        <List
          header={
            <div style={{ textAlign: "center" }}>
              <Button
                type="primary"
                onClick={e => {
                  e.preventDefault();
                  this.props.onUpdateUserList();
                }}
              >
                刷新用户列表
              </Button>
            </div>
          }
          bordered
          dataSource={this.props.userlist}
          renderItem={item =>
            item === this.props.user ? (
              <List.Item>
                <Switch
                  checked={this.state.admin}
                  onChange={checked => {
                    this.setState({ admin: checked });
                  }}
                  checkedChildren={<Icon type="user" />}
                  unCheckedChildren={<Icon type="user" />}
                />
                <CheckboxGroup
                  disabled={this.state.admin}
                  options={this.props.dirs}
                  value={this.state.userAuth}
                  onChange={checkedList => {
                    this.setState({ userAuth: checkedList });
                  }}
                />
                <Button
                  type="primary"
                  onClick={e => {
                    e.preventDefault();
                    this.props.onChangeUserAuth(
                      item,
                      this.state.admin
                        ? "*"
                        : JSON.stringify(this.state.userAuth)
                    );
                  }}
                >
                  ({item})修改权限
                </Button>
              </List.Item>
            ) : (
              <List.Item
                onClick={e => {
                  e.preventDefault();
                  this.props.onUpdateUserAuth(item);
                }}
              >
                {item}
              </List.Item>
            )
          }
        />

        <List
          style={{ margin: "30px 0 0" }}
          header={
            <div style={{ textAlign: "center" }}>
              <Button
                type="primary"
                onClick={e => {
                  e.preventDefault();
                  this.props.onGetInviteCode();
                }}
              >
                {this.props.cinvitecode
                  ? "(" + this.props.cinvitecode + ")"
                  : ""}获取验证码
              </Button>
            </div>
          }
          footer={
            <div style={{ textAlign: "center" }}>
              <Button
                type="primary"
                onClick={e => {
                  e.preventDefault();
                  this.props.onUpdateInviteCode();
                }}
                style={{ margin: "0 10px" }}
              >
                刷新列表
              </Button>
              <Button
                type="primary"
                onClick={e => {
                  e.preventDefault();
                  this.props.onClearInviteCode();
                }}
                style={{ margin: "0 10px" }}
              >
                清空邀请码
              </Button>
            </div>
          }
          bordered
          dataSource={this.props.invitecodelist}
          renderItem={item => <List.Item>{item}</List.Item>}
        />
      </div>
    ) : (
      <Redirect to="/" />
    );
  }
}
export default ChangeAuth;
