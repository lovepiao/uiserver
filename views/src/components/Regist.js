/**
 * 注册界面
 */

import React from "react";
import { Form, Input, Icon, Button, Alert, Row } from "antd";
import "./Login.less";

const FormItem = Form.Item;

const UserForm = ({ onRegist, err, form }) => (
  <Form
    onSubmit={e => {
      e.preventDefault();
      form.validateFields((err, values) => {
        if (!err) {
          onRegist(
            values.username,
            values.newpassword,
            values.agepassword,
            values.vercode
          );
        }
      });
    }}
    className="login-form"
  >
    {(() => {
      let items = [];
      const fields = ["username", "newpassword", "agepassword"];
      const msg = ["请输入用户名!", "请输入密码！", "请确认密码！"];
      const places = ["用户名", "密码", "确认密码"];
      for (let i = 0; i < 3; ++i) {
        items.push(
          <FormItem key={i}>
            {form.getFieldDecorator(fields[i], {
              rules: [{ required: true, message: msg[i] }]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25" }} />
                }
                type={i ? "password" : ""}
                placeholder={places[i]}
              />
            )}
          </FormItem>
        );
      }
      return items;
    })()}

    <FormItem>
      {form.getFieldDecorator("vercode", {
        rules: [{ required: true, message: "请输入邀请码！" }]
      })(
        <Input
          prefix={<Icon type="smile-o" style={{ color: "rgba(0,0,0,.25" }} />}
          placeholder="邀请码"
        />
      )}
    </FormItem>

    <FormItem>
      <Button type="primary" htmlType="submit" className="login-form-button">
        注 册
      </Button>
    </FormItem>
    <Alert
      type="error"
      message={err ? err : "空内容占高"}
      style={{ opacity: err ? 1.0 : 0 }}
      banner
    />
  </Form>
);

const Regist = Form.create()(UserForm);

export default ({ err, onRegist }) => (
  <Row
    type="flex"
    justify="space-around"
    align="middle"
    className="view-height"
  >
    <Regist err={err} onRegist={onRegist} />
  </Row>
);
