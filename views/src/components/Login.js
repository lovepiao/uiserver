/**
 * 登录界面
 */
import React from "react";
import { Link } from "react-router-dom";
import { Button, Form, Input, Icon, Checkbox, Row, Alert, message } from "antd";
import "./Login.less";

const FormItem = Form.Item;

let username = localStorage.getItem("username");
let password = localStorage.getItem("password");

const LoginForm = ({ err, onLogin, form }) => (
  <Form
    onSubmit={e => {
      e.preventDefault();
      form.validateFields((err, values) => {
        if (!err) {
          localStorage.setItem("remember", values.remember);
          username = values.username;
          password = values.password;
          localStorage.setItem(
            "username",
            values.remember ? values.username : ""
          );
          localStorage.setItem(
            "password",
            values.remember ? values.password : ""
          );
          onLogin(values.username, values.password);
        }
      });
    }}
    className="login-form"
  >
    <FormItem>
      {form.getFieldDecorator("username", {
        rules: [{ required: true, message: "请输入用户名!" }]
      })(
        <Input
          prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25" }} />}
          placeholder="用户名"
        />
      )}
    </FormItem>
    <FormItem>
      {form.getFieldDecorator("password", {
        rules: [{ required: true, message: "请输入密码!" }]
      })(
        <Input
          prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25" }} />}
          type="password"
          placeholder="密码"
        />
      )}
    </FormItem>
    <FormItem>
      {form.getFieldDecorator("remember", {
        valuePropName: "checked",
        initialValue: false
      })(<Checkbox>记住用户</Checkbox>)}
      <a
        className="login-form-forgot"
        href=""
        onClick={e => {
          e.preventDefault();
          message.warning("密码都会忘掉？我知道你只是跟我开玩笑！");
        }}
      >
        忘了密码？
      </a>
      <Button type="primary" htmlType="submit" className="login-form-button">
        登 录
      </Button>
      或 <Link to="/reg">现在注册！</Link>
    </FormItem>
    <Alert
      type="error"
      message={err ? err : "空内容占高"}
      style={{ opacity: err ? 1.0 : 0 }}
      banner
    />
  </Form>
);

const LoginFrame = Form.create({
  mapPropsToFields: () => ({
    username: Form.createFormField({ value: username }),
    password: Form.createFormField({ value: password }),
    remember: Form.createFormField({
      value: localStorage.getItem("remember") === "true"
    })
  })
})(LoginForm);

export default ({ err, onLogin }) => (
  <Row
    type="flex"
    justify="space-around"
    align="middle"
    className="view-height"
  >
    <LoginFrame err={err} onLogin={onLogin} />
  </Row>
);
