export const USER_LOGIN = "USER_LOGIN"; //登录
export const USER_LOGOUT = "USER_LOGOUT"; //注销
export const USER_REGIST = "USER_REGIST"; //注册
export const USER_CHANGEPW = "USER_CHANGEPW"; //修改密码
export const USER_GETINVITE = "USER_GETINVITE"; //获取邀请码
export const USER_UPDATEINVITE = "USER_UPDATEINVITE"; //更新邀请码
export const USER_CLEARINVITE = "USER_CLEARINVITE"; //清空邀请码
export const USER_UPDATELIST = ""; //刷新用户列表

export const DIRS_LOOK = "DIRS_LOOK"; //查看权限
export const DIRS_CHANGE = "DIRS_CHANGE"; //修改权限

export const ACTION_ASYNC = "ACTION_ASYNC"; //异步消息

export const uLogin = res => ({
  type: USER_LOGIN,
  res
});

export const uLogout = () => ({ type: USER_LOGOUT });

export const uRegist = res => ({ type: USER_REGIST, res });

export const uChangepw = res => ({ type: USER_CHANGEPW, res });

export const uGetInvite = res => ({ type: USER_GETINVITE, res });

export const uUpdateInvite = res => ({ type: USER_UPDATEINVITE, res });

export const uClearInvite = () => ({ type: USER_CLEARINVITE });

export const uUpdateList = res => ({ type: USER_UPDATELIST, res });

export const dLook = (user, res) => ({ type: DIRS_LOOK, user, res });

export const saga = (action, param = {}) => ({
  type: ACTION_ASYNC,
  action,
  param
});
