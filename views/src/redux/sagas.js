import { all, takeLatest, put, call } from "redux-saga/effects";
import {
  USER_LOGIN,
  uLogin,
  USER_LOGOUT,
  uLogout,
  ACTION_ASYNC,
  USER_CHANGEPW,
  uChangepw,
  USER_REGIST,
  uRegist,
  USER_GETINVITE,
  USER_UPDATEINVITE,
  uGetInvite,
  uUpdateInvite,
  USER_CLEARINVITE,
  uClearInvite,
  USER_UPDATELIST,
  uUpdateList,
  DIRS_LOOK,
  dLook,
  DIRS_CHANGE,
  saga
} from "./actions";
import axios from "axios";

axios.defaults.headers.post["Content-Type"] = "application/json";

function* iniRedux() {
  const res = yield call(axios.post, "/users/info", { timeout: 10000 });
  let info = res.data;
  if (info.id) {
    const res = yield call(axios.post, "/uires", {
      timeout: 10000
    });
    info.dirs = res.data;
  }
  yield put(uLogin(info));
}

function* login(action) {
  const res = yield call(
    axios.post,
    "/users/login",
    JSON.stringify({ username: action.user, password: action.password }),
    {
      timeout: 10000
    }
  );
  if (res.data.ret_code < 1000 || res.data.ret_code === 1100) {
    yield iniRedux();
  } else {
    yield put(uLogin(res.data));
  }
}

function* actionAsync(action) {
  console.log(action);
  switch (action.action) {
    case USER_LOGIN: {
      yield login(action.param);
      break;
    }
    case USER_CHANGEPW: {
      if (action.param.newpassword !== action.param.agepassword) {
        yield put(uChangepw({ ret_err: "新密码两次输入不相同" }));
      } else {
        const res = yield call(
          axios.post,
          "/users/chgpwd",
          JSON.stringify({
            oldpassword: action.param.oldpassword,
            newpassword: action.param.newpassword
          }),
          { timeout: 10000 }
        );
        yield put(uChangepw(res.data));
      }
      break;
    }
    case USER_REGIST: {
      if (action.param.password !== action.param.agepassword) {
        yield put(uRegist({ ret_err: "密码两次输入不相同" }));
      } else {
        const res = yield call(
          axios.post,
          "/users/reg",
          JSON.stringify({
            username: action.param.user,
            password: action.param.password,
            invitecode: Number(action.param.vercode)
          }),
          { timeout: 10000 }
        );
        yield put(uRegist(res.data));
      }
      break;
    }
    case USER_LOGOUT: {
      yield call(axios.post, "/users/logout", { timeout: 10000 });
      yield put(uLogout());
      break;
    }
    case USER_GETINVITE: {
      const res = yield call(axios.post, "/users/invitecode", {
        timeout: 10000
      });
      yield put(uGetInvite(res.data));
      break;
    }
    case USER_UPDATEINVITE: {
      const res = yield call(axios.post, "/users/icodelist", {
        timeout: 10000
      });
      yield put(uUpdateInvite(res.data));
      break;
    }
    case USER_CLEARINVITE: {
      const res = yield call(axios.post, "/users/clearcode", {
        timeout: 10000
      });
      if (res.data.ret_code < 1000) {
        yield put(uClearInvite());
      }
      break;
    }
    case USER_UPDATELIST: {
      const res = yield call(axios.post, "/users/userlist", {
        timeout: 10000
      });
      yield put(uUpdateList(res.data));
      break;
    }
    case DIRS_LOOK: {
      const res = yield call(
        axios.post,
        "/uires/get",
        JSON.stringify({
          username: action.param.user
        }),
        {
          timeout: 10000
        }
      );
      yield put(dLook(action.param.user,res.data));
      break;
    }
    case DIRS_CHANGE: {
      yield call(
        axios.post,
        "/uires/set",
        JSON.stringify({
          username: action.param.user,
          dirs: action.param.auth
        }),
        {
          timeout: 10000
        }
      );
      yield put(saga(DIRS_LOOK, { user: action.param.user }));
      break;
    }
    default:
      break;
  }
}

function* watchReduxAsync() {
  yield takeLatest(ACTION_ASYNC, actionAsync);
}

export default function* rootSaga() {
  yield all([iniRedux(), watchReduxAsync()]);
}
