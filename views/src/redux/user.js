import {
  USER_LOGOUT,
  USER_LOGIN,
  USER_CHANGEPW,
  USER_REGIST,
  USER_GETINVITE,
  USER_UPDATEINVITE,
  USER_CLEARINVITE,
  USER_UPDATELIST
} from "./actions";

export default (state = {}, action) => {
  console.log(action);
  switch (action.type) {
    case USER_LOGIN:
      return {
        ...state,
        user: action.res.id || "",
        auth: action.res.auth,
        err: action.res.ret_err || "",
        dirs: action.res.dirs || []
      };
    case USER_LOGOUT:
      return {
        ...state,
        user: "",
        auth: false,
        err: "",
        dirs: []
      };
    case USER_CHANGEPW:
      return {
        ...state,
        chgpwerr: action.res.ret_err || ""
      };
    case USER_REGIST:
      return { ...state, regerr: action.res.ret_err || "" };
    case USER_GETINVITE:
      return {
        ...state,
        invitecode: action.res.code,
        invitecodelist: action.res.list
      };
    case USER_UPDATEINVITE:
      return {
        ...state,
        invitecodelist: action.res.ret_err ? state.invitecodelist : action.res
      };
    case USER_CLEARINVITE:
      return {
        ...state,
        invitecodelist: []
      };
    case USER_UPDATELIST:
      return {
        ...state,
        userlist: action.res.ret_err ? state.userlist : action.res
      };
    default:
      return state;
  }
};
