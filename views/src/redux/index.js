import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Main from "../containers/Main";
import Regist from "../containers/Regist";
import Chgauth from "../containers/Chgauth";
import Chgpw from "../containers/Chgpw";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas'
import user from "./user";
import dirs from "./dirs";

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware]
const store = createStore(combineReducers({ user, dirs }),applyMiddleware(...middlewares));
sagaMiddleware.run(rootSaga);

export default () => (
  <Provider store={store}>
    <Router>
      <div>
        <Route exact path="/" component={Main} />
        <Route path="/reg" component={Regist} />
        <Route path="/chgpw" component={Chgpw} />
        <Route path="/chga" component={Chgauth} />
      </div>
    </Router>
  </Provider>
);
