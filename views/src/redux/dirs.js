import { DIRS_LOOK } from "./actions";

export default (state = {}, action) => {
  console.log(action);
  switch (action.type) {
    case DIRS_LOOK:
    return {
      ...state,
      dirs: action.res.ret_err?state.dirs:action.res,
      cuser: action.user
    }
    default:
      return state;
  }
};
